<?php
/**
 * @file
 * Enables modules and site configuration for a standard site installation.
 */

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function ebiz_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}


/**
* Implements hook_install_tasks_alter()
*/
function ebiz_install_tasks_alter(&$tasks, $install_state) {

  // Magically go one level deeper in solving years of dependency problems
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_load_profile']['function'] = 'panopoly_core_install_load_profile';
}

/**
* Implements hook_install_tasks().
*/
function ebiz_install_tasks($install_state) {
  $tasks =  array(
    'profile_settings' => array(
      'display_name' => st('Setup Profile'),
      'type' => 'form',
    ),
  );

  // Add the Panopoly App Server to the Installation Process
// require_once(drupal_get_path('module', 'apps') . '/apps.profile.inc');
 // $tasks = $tasks + apps_profile_install_tasks($install_state, array('machine name' => 'i18n', 'default apps' => array('i18n')));

  return $tasks;
}
function profile_settings() {
  $form = array();
$arr[0]=st("contrat avec optimisation");
$arr[1]=st("contrat avec engagements sur des resultats");
  $form['optimisation'] = array(
    '#type' => 'radios',
    '#title' => st('Optimisation du site'),
	'#options'=>$arr
  );
  $form['multilangue'] = array(
    '#type' => 'checkbox',
    '#title' => st('Multilangue'),
  );
    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => st('Continue'),
  );
  return $form;
}
function profile_settings_submit($form, &$form_state) {
  $values = $form_state['values'];
if($values['multilangue']==1){variable_set("ebiz_multilangue",1);$modules[]='i18n';
    $projects['i18n'] = "http://ftp.drupal.org/files/projects/i18n-7.x-1.10.zip";}
 if($values['optimisation']==1){variable_set("ebiz_optimisation",1);$modules[]='metatag';
     $projects['metatag'] = "http://ftp.drupal.org/files/projects/metatag-7.x-1.0-beta7.zip";

}
// download_modules();
$enable_dependencies = TRUE; // Whether or not to enable dependant modules

module_enable($modules, $enable_dependencies);
}


function download_modules() {
module_load_include('inc', 'update', 'update.manager');
    $projects['util'] = "http://ftp.drupal.org/files/projects/util-7.x-1.0-rc1.zip";
    $projects['weight'] = "http://ftp.drupal.org/files/projects/weight-7.x-2.3.zip";
 foreach ($projects as $prj => $url) {
  if ($url) {
    $local_cache = update_manager_file_get($url);
    if (!$local_cache) {
       drupal_set_message( t('Unable to retrieve Drupal project from %url.', array('%url' => $url)));
      return;
    }
  }


  $directory = _update_manager_extract_directory();
  try {
    $archive = update_manager_archive_extract($local_cache, $directory);
  }
  catch (Exception $e) {
    drupal_set_message(t('Provided archive contains no files.'));
    return;
  }

  $files = $archive->listContents();
  if (!$files) {
    drupal_set_message(t('Provided archive contains no files.'));
    return;
  }

  // Unfortunately, we can only use the directory name to determine the project
  // name. Some archivers list the first file as the directory (i.e., MODULE/)
  // and others list an actual file (i.e., MODULE/README.TXT).
  $project = strtok($files[0], '/\\');

  $archive_errors = update_manager_archive_verify($project, $local_cache, $directory);
  if (!empty($archive_errors)) {
     drupal_set_message(array_shift($archive_errors));
    // @todo: Fix me in D8: We need a way to set multiple errors on the same
    // form element and have all of them appear!
    if (!empty($archive_errors)) {
      foreach ($archive_errors as $error) {
        drupal_set_message($error, 'error');
      }
    }
    return;
  }

  // Make sure the Updater registry is loaded.
  drupal_get_updaters();

  $project_location = $directory . '/' . $project;
  try {
    $updater = Updater::factory($project_location);
  }
  catch (Exception $e) {
    drupal_set_message( $e->getMessage());
    return;
  }

  try {
    $project_title = Updater::getProjectTitle($project_location);
  }
  catch (Exception $e) {
    drupal_set_message( $e->getMessage());
    return;
  }

  if (!$project_title) {
    drupal_set_message( t('Unable to determine %project name.', array('%project' => $project)));
  }

  if ($updater->isInstalled()) {
    drupal_set_message( t('%project is already installed.', array('%project' => $project_title)));
    return;
  }

  $project_real_location = drupal_realpath($project_location);
  $arguments[] = array(
    'project' => $project,
    'updater_name' => get_class($updater),
    'local_url' => $project_real_location,
  );
}

    system_authorized_init('update_authorize_run_install', drupal_get_path('module', 'update') . '/update.authorize.inc', $arguments, t('Update manager'));
    drupal_goto(system_authorized_get_url());
}
