api = 2
core = 7.x
; Include the definition for how to build Drupal core directly, including patches:
includes[] = drupal-org-core.make
; Download the ebiz install profile and recursively build all its dependencies:
projects[ebiz][type] = profile
projects[ebiz][download][type] = git
projects[ebiz][download][branch] = "master"
projects[ebiz][download][url] = http://git.drupal.org/sandbox/lara/2086707.git
projects[ebiz][download][directory_name]="ebiz"
