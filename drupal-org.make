; This is a standard make file for packaging the distribution along with any
; contributed modules/themes or external libraries. Some examples are below.
; See http://drupal.org/node/159730 for more details.

api = 2
core = 7.x

; Contributed modules; standard.



projects[json2][type] = module
projects[json2][version] = 1.1
projects[json2][subdir] = contrib


projects[picture][type] = module
projects[picture][version] = 1.1
projects[picture][subdir] = contrib


; Contributed projects; Sparkish.
projects[ckeditor][type] = module
projects[ckeditor][version] = 1.13
projects[ckeditor][subdir] = contrib



projects[edit][type] = module
projects[edit][version] = 1.0-alpha11
;projects[edit][download][type] = git
;projects[edit][download][branch] = 7.x-1.x
projects[edit][subdir] = contrib



projects[gridbuilder][type] = module
projects[gridbuilder][version] = 1.0-alpha2
;projects[gridbuilder][download][type] = git
;projects[gridbuilder][download][branch] = 7.x-1.x
projects[gridbuilder][subdir] = contrib

projects[layout][type] = module
projects[layout][version] = 1.0-alpha6
;projects[layout][download][type] = git
;projects[layout][download][branch] = 7.x-1.x
projects[layout][subdir] = contrib

projects[every_field][type] = module
projects[every_field][version] = 1.x-dev
projects[every_field][subdir] = contrib

projects[backup_migrate][version] = "2.7"
projects[backup_migrate][subdir] = contrib
projects[backup_migrate][type] = module

;projects[backports][version] = "1.0-alpha1"
;projects[backports][subdir] = contrib
;projects[backports][type] = module

projects[userprotect][version] = "1.0"
projects[userprotect][subdir] = contrib
projects[userprotect][type] = module

projects[transliteration][version] = "3.1"
projects[transliteration][subdir] = contrib
projects[transliteration][type] = module

projects[views_php][version] = "1.x-dev"
projects[views_php][subdir] = contrib
projects[views_php][type] = module

projects[views_ui_basic][version] = "1.2"
projects[views_ui_basic][subdir] = contrib
projects[views_ui_basic][type] = module

;projects[computed_field][version] = "1.0-beta1"
;projects[computed_field][subdir] = contrib
;projects[computed_field][type] = module

;projects[simplify][version] = "3.1"
;projects[simplify][subdir] = contrib
;projects[simplify][type] = module

projects[custom_search][version] = "1.13"
projects[custom_search][subdir] = contrib
projects[custom_search][type] = module

projects[webform][version] = "3.19"
projects[webform][subdir] = contrib
projects[webform][type] = module

projects[tabtamer][version] = "1.1"
projects[tabtamer][subdir] = contrib
projects[tabtamer][type] = module

projects[site_map][version] = "1.0"
projects[site_map][subdir] = contrib
projects[site_map][type] = module

projects[rules][version] = "2.5"
projects[rules][subdir] = contrib
projects[rules][type] = module

projects[linkchecker][version] = "1.1"
projects[linkchecker][subdir] = contrib
projects[linkchecker][type] = module

;projects[linkit][version] = "2.6"
;projects[linkit][subdir] = contrib
;projects[linkit][type] = module

projects[login_destination][version] = "1.1"
projects[login_destination][subdir] = contrib
projects[login_destination][type] = module

projects[hierarchical_select][version] = "3.0-alpha6"
projects[hierarchical_select][subdir] = contrib
projects[hierarchical_select][type] = module

projects[references][version] = "2.1"
projects[references][subdir] = contrib
projects[references][type] = module

projects[honeypot][version] = "1.15"
projects[honeypot][subdir] = contrib
projects[honeypot][type] = module

projects[fitvids][version] = "1.14"
projects[fitvids][subdir] = contrib
projects[fitvids][type] = module

;projects[image_resize_filter][version] = "1.13"
;projects[image_resize_filter][subdir] = contrib
;projects[image_resize_filter][type] = module

projects[email][version] = "1.2"
projects[email][subdir] = contrib
projects[email][type] = module

projects[duplicate_role][version] = "1.2"
projects[duplicate_role][subdir] = contrib
projects[duplicate_role][type] = module

;projects[caption_filter][version] = "1.2"
;projects[caption_filter][subdir] = contrib
;projects[caption_filter][type] = module


projects[addthis][version] = "4.0-alpha2"
projects[addthis][subdir] = contrib
projects[addthis][type] = module

; Contributed modules; UX++





;panopoly
projects[panopoly_core][version] = 1.0-rc5
projects[panopoly_core][subdir] = panopoly
projects[panopoly_core][download][branch] = 7.x-1.x
projects[panopoly_core][download][type] = git


projects[panopoly_images][version] = 1.0-rc5
projects[panopoly_images][subdir] = panopoly

projects[panopoly_theme][version] = 1.0-rc5
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.0-rc5
projects[panopoly_magic][subdir] = panopoly
projects[panopoly_magic][download][branch] = 7.x-1.x
projects[panopoly_magic][download][type] = git


projects[panopoly_widgets][version] = 1.0-rc5
projects[panopoly_widgets][subdir] = panopoly

projects[panopoly_admin][version] = 1.0-rc5
projects[panopoly_admin][subdir] = panopoly
projects[panopoly_admin][download][branch] = 7.x-1.x
projects[panopoly_admin][download][type] = git


projects[panopoly_users][version] = 1.0-rc5
projects[panopoly_users][subdir] = panopoly

projects[panopoly_pages][version] = 1.0-rc5
projects[panopoly_pages][subdir] = panopoly
projects[panopoly_pages][download][branch] = 7.x-1.x
projects[panopoly_pages][download][type] = git

projects[panopoly_wysiwyg][version] = 1.0-rc5
projects[panopoly_wysiwyg][subdir] = panopoly

projects[panopoly_search][version] = 1.0-rc5
projects[panopoly_search][subdir] = panopoly


; Themes
;projects[omega][version] = "4.0"
projects[omega][type] = theme
projects[omega][subdir] = contrib

; Libraries.
; NOTE: These need to be listed in http://drupal.org/packaging-whitelist.
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.0.1/ckeditor_4.0.1_standard.zip

;libraries[json2][download][type] = get
;libraries[json2][download][url] = https://raw.github.com/douglascrockford/JSON-js/master/json2.js
;libraries[json2][revision] = fc535e9cc8fa78bbf45a85835c830e7f799a5084

;libraries[fitvids][directory_name] = "fitvids"
;libraries[fitvids][type] = "library"
;libraries[fitvids][destination] = "libraries"
;libraries[fitvids][download][type] = "get"
;libraries[fitvids][download][url] = "https://github.com/davatron5000/FitVids.js/archive/master.zip" ; TODO add download URI

libraries[json2][directory_name] = "json2"
libraries[json2][download][type] = "get"
libraries[json2][destination] = "libraries"
libraries[json2][download][url] = https://raw.github.com/douglascrockford/JSON-js/master/json2.js
libraries[json2][revision] = fc535e9cc8fa78bbf45a85835c830e7f799a5084